package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.Offices;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficesRepository extends JpaRepository <Offices,Integer> {
    
}
