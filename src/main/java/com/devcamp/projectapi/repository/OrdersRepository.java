package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.Orders;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdersRepository extends JpaRepository <Orders,Integer>  {
    
}
