package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.ProductLines;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductLinesRepository extends JpaRepository <ProductLines,Integer>  {
    
}
