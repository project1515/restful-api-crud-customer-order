package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.Payments;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentsRepository extends JpaRepository <Payments,Integer>  {
    
}
