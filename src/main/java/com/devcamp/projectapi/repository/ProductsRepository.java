package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.Products;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductsRepository extends JpaRepository <Products,Integer>  {
    
}
