package com.devcamp.projectapi.interfacelist;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public interface AllErrorHandler {

    @ControllerAdvice
    public class ErrorHandler extends ResponseEntityExceptionHandler {
    // error handle for @Valid
        @Override
        protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                HttpHeaders headers, HttpStatus status, WebRequest request) {
        
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", status.value());
            // Get all errors
            List<String> errors = ex.getBindingResult().getFieldErrors().stream()
                    .map(x -> x.getDefaultMessage()).collect(Collectors.toList());
            body.put("errors", errors);
        
            return new ResponseEntity<>(body, headers, status);
        }

        @ExceptionHandler({MethodArgumentTypeMismatchException.class})
        public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex, WebRequest request) {
    
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.BAD_REQUEST);
    
            String error = "Giá trị " + ex.getValue() + " của path variable " + 
            ex.getName() + " không phải là loại biến " + ex.getRequiredType().getSimpleName();
    
            body.put("error", error);
    
            return new ResponseEntity<Object>(
            body, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @ResponseStatus (value = HttpStatus.NOT_FOUND)
    public class ResourceNotFound extends Exception {
        public ResourceNotFound(String errorMessage) {
            super(errorMessage);
        }
    }


    @ResponseStatus (value = HttpStatus.NOT_ACCEPTABLE)
    public class InputNotAccept extends Exception {
        public InputNotAccept(String errorMessage) {
            super(errorMessage);
        }
    }

    @ResponseStatus (value = HttpStatus.TOO_MANY_REQUESTS)
    public class TooManyRequest extends Exception {
        public TooManyRequest(String errorMessage) {
            super(errorMessage);
        }
    }

    @ResponseStatus (value = HttpStatus.CONFLICT)
    public class ConflictData extends Exception {
        public ConflictData(String errorMessage) {
            super(errorMessage);
        }
    }

}
