package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.model.Employees;
import com.devcamp.projectapi.repository.EmployeesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class EmployessController {
    @Autowired
    EmployeesRepository employeesRepository;

    @GetMapping("/employess")
    public ResponseEntity<Object> getAllEmployess() {
        return new ResponseEntity<Object>(employeesRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/employess/{id}")
    public ResponseEntity<Object> getEmployessById(@PathVariable Integer id) {
        if (employeesRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(employeesRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/employess")
    public ResponseEntity<Object> createEmployess(@Valid @RequestBody Employees employees) {
        try {
            return new ResponseEntity<Object>(employeesRepository.save(employees), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employess: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/employess/{id}")
    public ResponseEntity<Object> updateEmployess(@PathVariable Integer id, @Valid @RequestBody Employees employees){
        Optional<Employees> _employess = employeesRepository.findById(id);
        if(_employess.isPresent()){
            _employess.get().setFirstName(employees.getFirstName());
            _employess.get().setLastName(employees.getLastName());
            _employess.get().setExtension(employees.getExtension());
            _employess.get().setEmail(employees.getEmail());
            _employess.get().setOfficeCode(employees.getOfficeCode());
            _employess.get().setReportTo(employees.getReportTo());
            _employess.get().setJobTitle(employees.getJobTitle());
            try {
                return new ResponseEntity<Object>(employeesRepository.save(_employess.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Employess: " + e.getCause().getCause().getMessage());
            }
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/employess/{id}")
    public ResponseEntity<Object> deleteEmployess(@PathVariable Integer id){
        if(employeesRepository.findById(id).isPresent()){
            employeesRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
