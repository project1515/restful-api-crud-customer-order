package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.model.Payments;
import com.devcamp.projectapi.repository.CustomersRepository;
import com.devcamp.projectapi.repository.PaymentsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class PaymentController {

    @Autowired
    PaymentsRepository paymentsRepository;

    @Autowired
    CustomersRepository customersRepository;

    @GetMapping("/payments")
    public ResponseEntity<Object> getAllPayments() {
        return new ResponseEntity<Object>(paymentsRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/payments/{id}")
    public ResponseEntity<Object> getPaymentById(@PathVariable Integer id) {
        if (paymentsRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(paymentsRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/customers/{id}/payments")
    public ResponseEntity<Object> getAllPaymentByCustomerId(@PathVariable Integer id){
        if (customersRepository.findById(id).isPresent()){
            return new ResponseEntity<Object>(customersRepository.findById(id).get().getPayments(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/customers/{id}/payments")
    public ResponseEntity<Object> createPayment(@PathVariable Integer id, @Valid @RequestBody Payments payments) {
        if (customersRepository.findById(id).isPresent()) {
            try {
                payments.setCustomers(customersRepository.findById(id).get());
                return new ResponseEntity<Object>(paymentsRepository.save(payments), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified Payment: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/payments/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable Integer id,
            @Valid @RequestBody Payments payments) {
        Optional<Payments> _payment = paymentsRepository.findById(id);
        if (_payment.isPresent()) {
            _payment.get().setCheckNumber(payments.getCheckNumber());
            _payment.get().setPaymentDate(payments.getPaymentDate());
            _payment.get().setAmmount(payments.getAmmount());
            try {
                return new ResponseEntity<Object>(paymentsRepository.save(_payment.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Object> deletePayment(@PathVariable Integer id) {
        if (paymentsRepository.findById(id).isPresent()) {
            paymentsRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
