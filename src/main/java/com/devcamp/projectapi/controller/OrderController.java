package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;
import com.devcamp.projectapi.model.Orders;
import com.devcamp.projectapi.repository.OrdersRepository;
import com.devcamp.projectapi.repository.CustomersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OrderController {

    @Autowired
    OrdersRepository ordersRepository;

    @Autowired
    CustomersRepository customersRepository;

    @GetMapping("/orders")
    public ResponseEntity<Object> getAllOrders() {
        return new ResponseEntity<Object>(ordersRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable Integer id) {
        if (ordersRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(ordersRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/customers/{id}/orders")
    public ResponseEntity<Object> getAllOrderByCustomerId(@PathVariable Integer id){
        if (customersRepository.findById(id).isPresent()){
            return new ResponseEntity<Object>(customersRepository.findById(id).get().getOrders(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/customers/{id}/orders")
    public ResponseEntity<Object> createOrder(@PathVariable Integer id, @Valid @RequestBody Orders orders) {
        if (customersRepository.findById(id).isPresent()) {
            try {
                orders.setCustomers(customersRepository.findById(id).get());
                return new ResponseEntity<Object>(ordersRepository.save(orders), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable Integer id,
            @Valid @RequestBody Orders orders) {
        Optional<Orders> _order = ordersRepository.findById(id);
        if (_order.isPresent()) {
            _order.get().setOrderDate(orders.getOrderDate());
            _order.get().setRequiredDate(orders.getRequiredDate());
            _order.get().setShippedDate(orders.getShippedDate());
            _order.get().setStatus(orders.getStatus());
            _order.get().setComments(orders.getComments());
            try {
                return new ResponseEntity<Object>(ordersRepository.save(_order.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable Integer id) {
        if (ordersRepository.findById(id).isPresent()) {
            ordersRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
