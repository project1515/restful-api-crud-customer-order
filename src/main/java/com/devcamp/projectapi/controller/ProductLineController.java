package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.model.ProductLines;
import com.devcamp.projectapi.repository.ProductLinesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProductLineController {

    @Autowired
    ProductLinesRepository productLinesRepository;

    @GetMapping("/productlines")
    public ResponseEntity<Object> getAllProductLines() {
        return new ResponseEntity<Object>(productLinesRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/productlines/{id}")
    public ResponseEntity<Object> getProductLineById(@PathVariable Integer id) {
        if (productLinesRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(productLinesRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/productlines")
    public ResponseEntity<Object> createProductLine(@Valid @RequestBody ProductLines productLines) {
        try {
            return new ResponseEntity<Object>(productLinesRepository.save(productLines), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified ProductLine: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/productlines/{id}")
    public ResponseEntity<Object> updateProductLine(@PathVariable Integer id,@Valid @RequestBody ProductLines productLines) {
        Optional<ProductLines> _productline = productLinesRepository.findById(id);
        if(_productline.isPresent()){
            _productline.get().setProductLine(productLines.getProductLine());
            _productline.get().setDescription(productLines.getDescription());
            try {
                return new ResponseEntity<Object>(productLinesRepository.save(_productline.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified ProductLine: " + e.getCause().getCause().getMessage());
            }
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/productlines/{id}")
    public ResponseEntity<Object> deleteProductLine(@PathVariable Integer id) {
        if (productLinesRepository.findById(id).isPresent()) {
            productLinesRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
