package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.model.Offices;
import com.devcamp.projectapi.repository.OfficesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OfficeController {
    @Autowired
    OfficesRepository officesRepository;

    @GetMapping("/offices")
    public ResponseEntity<Object> getAllOffices() {
        return new ResponseEntity<Object>(officesRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/offices/{id}")
    public ResponseEntity<Object> getOfficeById(@PathVariable Integer id) {
        if (officesRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(officesRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/offices")
    public ResponseEntity<Object> createOffice(@Valid @RequestBody Offices offices) {
        try {
            return new ResponseEntity<Object>(officesRepository.save(offices), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Office: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/offices/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable Integer id, @Valid @RequestBody Offices offices) {
        Optional<Offices> _office = officesRepository.findById(id);
        if (_office.isPresent()) {
            _office.get().setCity(offices.getCity());
            _office.get().setPhone(offices.getPhone());
            _office.get().setAddressLine(offices.getAddressLine());
            _office.get().setState(offices.getState());
            _office.get().setCountry(offices.getCountry());
            _office.get().setTerritory(offices.getTerritory());
            try {
                return new ResponseEntity<Object>(officesRepository.save(_office.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Employess: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Object> deleteOffice(@PathVariable Integer id) {
        if (officesRepository.findById(id).isPresent()) {
            officesRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
