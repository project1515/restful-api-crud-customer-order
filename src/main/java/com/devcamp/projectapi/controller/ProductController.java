package com.devcamp.projectapi.controller;

import java.util.Optional;

import com.devcamp.projectapi.model.Products;
import com.devcamp.projectapi.repository.ProductLinesRepository;
import com.devcamp.projectapi.repository.ProductsRepository;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProductController {
    
    @Autowired
    ProductLinesRepository productLinesRepository;

    @Autowired
    ProductsRepository productsRepository;

    @GetMapping("/products")
    public ResponseEntity<Object> getAllProducts() {
        return new ResponseEntity<Object>(productsRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable Integer id) {
        if (productsRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(productsRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/productlines/{id}/products")
    public ResponseEntity<Object> createProduct(@PathVariable Integer id, @Valid @RequestBody Products products) {
        if (productLinesRepository.findById(id).isPresent()) {
            try {
                products.setProductLines(productLinesRepository.findById(id).get());
                return new ResponseEntity<Object>(productsRepository.save(products), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified Product: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable Integer id,
            @Valid @RequestBody Products products) {
        Optional<Products> _product = productsRepository.findById(id);
        if (_product.isPresent()) {
            _product.get().setProductCode(products.getProductCode());
            _product.get().setProductName(products.getProductName());
            _product.get().setProductDescription(products.getProductDescription());
            _product.get().setProductScale(products.getProductScale());
            _product.get().setProductVendor(products.getProductVendor());
            _product.get().setQuantityInStock(products.getQuantityInStock());
            _product.get().setBuyPrice(products.getBuyPrice());
            try {
                return new ResponseEntity<Object>(productsRepository.save(_product.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Product: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable Integer id) {
        if (productsRepository.findById(id).isPresent()) {
            productsRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
