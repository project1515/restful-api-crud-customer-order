package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;
import com.devcamp.projectapi.model.OrderDetails;
import com.devcamp.projectapi.repository.OrderDetailsRepository;
import com.devcamp.projectapi.repository.OrdersRepository;
import com.devcamp.projectapi.repository.ProductsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OrderDetailController {
    @Autowired
    OrderDetailsRepository orderDetailsRepository;

    @Autowired
    OrdersRepository ordersRepository;

    @Autowired
    ProductsRepository productsRepository;

    @GetMapping("/orderdetails")
    public ResponseEntity<Object> getAllOrderDetail() {
        return new ResponseEntity<Object>(orderDetailsRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/orders/{id}/orderdetails")
    public ResponseEntity<Object> getAllOrderDetailByOrderId(@PathVariable Integer id){
        if (ordersRepository.findById(id).isPresent()){
            return new ResponseEntity<Object>(ordersRepository.findById(id).get().getOrderDetails(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/orderdetails/{id}")
    public ResponseEntity<Object> getOrderDetailById(@PathVariable Integer id) {
        if (orderDetailsRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(orderDetailsRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/orders/{orderId}/products/{productId}/orderdetails")
    public ResponseEntity<Object> createOrderDetail(@PathVariable(required = true) Integer orderId,
            @PathVariable(required = true) Integer productId,
            @Valid @RequestBody OrderDetails orderDetails) {
        if (ordersRepository.findById(orderId).isPresent() && productsRepository.findById(productId).isPresent()) {
            try {
                orderDetails.setOrders(ordersRepository.findById(orderId).get());
                orderDetails.setProducts(productsRepository.findById(productId).get());
                return new ResponseEntity<Object>(orderDetails, HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified OrderDetail: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>("Không tìm thấy order và product để tạo orderdetail",
                    HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/orderdetails/{id}")
    public ResponseEntity<Object> updateOrderDetail(@PathVariable Integer id,
            @Valid @RequestBody OrderDetails orderDetails) {
        Optional<OrderDetails> _orderDetail = orderDetailsRepository.findById(id);
        if (_orderDetail.isPresent()) {
            _orderDetail.get().setQuantityOrder(orderDetails.getQuantityOrder());
            _orderDetail.get().setPriceEach(orderDetails.getPriceEach());
            try {
                return new ResponseEntity<Object>(orderDetailsRepository.save(_orderDetail.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified OrderDetail: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orderdetails/{id}")
    public ResponseEntity<Object> deleteOrderDetail(@PathVariable Integer id) {
        if (orderDetailsRepository.findById(id).isPresent()) {
            orderDetailsRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
