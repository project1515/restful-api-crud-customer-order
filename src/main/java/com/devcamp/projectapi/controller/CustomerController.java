package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.model.Customers;
import com.devcamp.projectapi.repository.CustomersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerController {
    @Autowired
    CustomersRepository customersRepository;

    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomer() {
        return new ResponseEntity<Object>(customersRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable Integer id) {
        if (customersRepository.findById(id).isPresent()) {
            return new ResponseEntity<Object>(customersRepository.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customers customers) {
        try {
            return new ResponseEntity<Object>(customersRepository.save(customers), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable Integer id, @Valid @RequestBody Customers customers) {
        Optional<Customers> _customers = customersRepository.findById(id);
        if (_customers.isPresent()) {
            _customers.get().setFirstName(customers.getFirstName());
            _customers.get().setLastName(customers.getLastName());
            _customers.get().setPhoneNumber(customers.getPhoneNumber());
            _customers.get().setAddress(customers.getAddress());
            _customers.get().setCity(customers.getCity());
            _customers.get().setState(customers.getState());
            _customers.get().setPostalCode(customers.getPostalCode());
            _customers.get().setCountry(customers.getCountry());
            _customers.get().setSalesRepEmployeeNumber(customers.getSalesRepEmployeeNumber());
            _customers.get().setCreditLimit(customers.getCreditLimit());
            try {
                return new ResponseEntity<Object>(customersRepository.save(_customers.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Customer: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable Integer id){
        if(customersRepository.findById(id).isPresent()){
            customersRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
