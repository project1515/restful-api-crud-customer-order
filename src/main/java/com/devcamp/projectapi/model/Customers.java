package com.devcamp.projectapi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "customers")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Customers {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Column (length = 50,nullable = false)
    private String firstName;
    
    @Column (length = 50,nullable = false)
    private String lastName;

    @Column (length = 50,nullable = false)
    private String phoneNumber;

    private String address;

    @Column (length = 50)
    private String city;

    @Column (length = 50)
    private String state;

    @Column (length = 50)
    private String postalCode;

    @Column (length = 50)
    private String country;

    @Column (nullable = true)
    private int salesRepEmployeeNumber;

    @Column (nullable = true)
    private int creditLimit;

    @OneToMany (fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "customers")
    private List <Payments> payments;

    @OneToMany (fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "customers")
    private List <Orders> orders;
}
