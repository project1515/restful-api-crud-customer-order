package com.devcamp.projectapi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "products",uniqueConstraints = {@UniqueConstraint(name = "uk_product_code",columnNames = "product_code")}) // uniqueConstrants : tạo phần tử với key unique với tên key có thể tự chọn , chọn nhiều cột trong 1 key unique nếu muốn (thêm cột sau columnName cách bởi dấu phẩy)
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Products {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Column (length = 50,name = "product_code",nullable = false) // length : độ dài phần từ
    private String productCode;
    
    @Column (nullable = false)
    private String productName;

    @Column (length = 2500)
    private String productDescription;

    @ManyToOne (fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn (name = "product_line_id",foreignKey = @ForeignKey (name="product_line_id")) // Set tên khoá ngoại
    private ProductLines productLines;

    @Column (length = 50,name="product_scale",nullable = false)
    private String productScale;

    @Column (length = 50,nullable = false)
    private String productVendor;
    
    @Column (name="quantity_in_stock",nullable = false)
    private int quantityInStock;

    @Column (name = "buy_price",nullable = false,columnDefinition = "Decimal(10,2)")
    private double buyPrice;

    // @Column (precision = 10,scale = 2)
    // private BigDecimal buyPrice;

    @OneToMany (fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "products")
    private List <OrderDetails> orderDetails;

}
