package com.devcamp.projectapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "offices")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Offices {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Column (length = 50,nullable = false)
    private String city;

    @Column (length = 50,nullable = false)
    private String phone;

    @Column (name = "address_line",nullable = false)
    private String addressLine;

    @Column (length = 50)
    private String state;

    @Column (length = 50,nullable = false)
    private String country;

    @Column (length = 50)
    private String territory;
}
